package com.example.cryptoprice.controller;

import com.example.cryptoprice.service.CryptoService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequiredArgsConstructor
public class CryptoPriceController {

    private final CryptoService cryptoService;

    @ResponseBody
    @GetMapping("/price")
    public Map<String, Double> getCryptoPrice() {
        return cryptoService.getCryptoPrices();
    }
}
