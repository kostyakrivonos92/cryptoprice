package com.example.cryptoprice.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Service
@RequiredArgsConstructor
public class CryptoService {

    private final Map<String, Double> cryptoPrices = new ConcurrentHashMap<>();

    //we can use any other library for api templates connection
    private final RestTemplate restTemplate;

    @Value("${binance.api.url}")
    private String BINANCE_API_URL;

    @Value("${crypto.currencies}")
    private String[] symbols;

    // Асинхронный метод для обновления цен из Binance API
    @Scheduled(initialDelay = 60_000, fixedRate = 60_000) // обновление цен каждую минуту
    public void updateCryptoPricesAsync() {
        for (String symbol : symbols) {
            CompletableFuture.supplyAsync(() -> {
                String url = BINANCE_API_URL + symbol;
                try {
                    var response = restTemplate.getForObject(url, Map.class);
                    if (response != null && response.containsKey("markPrice")) {
                        double markPrice = Double.parseDouble(response.get("markPrice").toString());
                        cryptoPrices.put(symbol, markPrice);
                        log.info("Updated cryptoPrices for {}", symbol);
                    }
                } catch (Exception e) {
                    log.error("Error updating price for {}: {}", symbol, e.getMessage());
                }
                return null;
            });
        }
    }

    public Map<String, Double> getCryptoPrices() {
        return cryptoPrices;
    }
}
